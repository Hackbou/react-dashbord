import React from 'react'
import { ArrowBack, Home } from '@mui/icons-material'
import './style.scss'
import { render } from '@testing-library/react'

const SideMenu = () => {

  const moi = [
    {
      home: render(<Home />)
    }
  ]

  return (
    <div className="side-menu">
        <Home className='side-menu-icon'/>
        <h3>Acceuil</h3>
        <ArrowBack className='side-menu-deroulante-icone' />
        {moi.home}
    </div>
  )
}

export default SideMenu 
