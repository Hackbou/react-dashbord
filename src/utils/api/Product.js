export const Products = [
    {
        id: 1,
        libelle: 'Jupe',
        image: 'assets/products/product_1.jpg',
        price: 2500,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 2,
        libelle: 'Nike',
        image: 'assets/products/product_2.jpg',
        price: 9900,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 3,
        libelle: 'Jumper',
        image: 'assets/products/product_3.jpg',
        price: 2500,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 4,
        libelle: 'Pull',
        image: 'assets/products/product_4.jpg',
        price: 5500,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 5,
        libelle: 'Chemise Noire',
        image: 'assets/products/product_5.jpg',
        price: 4000,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 6,
        libelle: 'Sous-vetement',
        image: 'assets/products/product_6.jpg',
        price: 1000,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 7,
        libelle: 'Robe rouge',
        image: 'assets/products/product_7.jpg',
        price: 35000,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    },
    {
        id: 8,
        libelle: 'Filama Choes',
        image: 'assets/products/product_8.jpg',
        price: 5900,
        description: 'Vel justo sea. Diam invidunt labore dolore. Ea at diam sadipscing nonumy stet takimata stet. Takimata quis ullamcorper aliquam consetetur lorem minim eum ipsum amet sed diam vero et magna diam amet eos accusam. Ea dolores clita diam ipsum labore sed aliquip ut. Placerat assum magna tempor invidunt gubergren nulla no at ipsum no sit duo velit dolore consequat sea et.'
    }
]