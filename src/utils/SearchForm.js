import React from 'react'
import { Search } from '@mui/icons-material'
import './style.scss'

export default function SearchForm() {
  return (
    <div className='search-form'>
        <Search className='search-form-icon'/>
        <input type="text" placeholder='Search...' />
    </div>
  )
}
