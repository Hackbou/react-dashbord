import React from "react"
import SearchForm from "../../utils/SearchForm"
import SideMenu from "../../utils/SideMenu"
import "./sidebar.scss"

export default function Sidebar() {
  return (
    <div className="sidebar-container">
      <div className="sidebar-logo">
          <h3>CNS Admin dashboard</h3>
      </div>
      <SearchForm />
      <hr />
      <SideMenu />
    </div>
  )
}
