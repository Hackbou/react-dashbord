import React from 'react'
import Sidebar from '../components/sidebar/Sidebar'

export default function Router() {
  return (
    <div>
        <Sidebar />
    </div>
  )
}
